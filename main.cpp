#include <iostream>
#include <string>
#include"FileSystem.h"

using namespace std;

int main() {

	FileSystem fs;
	string cmdLine;
	fs.DoCommand("help");
	while (true)
	{
		fs.WaitCommand();
		cmdLine = "";
		getline(cin, cmdLine);
		fs.DoCommand(cmdLine);
	}
}