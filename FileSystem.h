#pragma once
#include <vector>
#include <string>
#include <sstream>
#include <iostream>
#include <fstream>

using namespace std;

class file
{
public:
	string fileName;
	string content;
	file();
	file(string fileName, string content);
	~file();
	void updatfileName(string newfileName);

private:
	
};

file::file()
{
}
file::file(string fileName,string content) {
	this->fileName = fileName;
	this->content = content;
}
void file::updatfileName(string newfilename) {
	this->fileName = newfilename;
}

file::~file()
{
}
class User {
public:
	string name;
	string password;
	User() {

	}
	User(string name, string password) {
		this->name = name;
		this->password = password;
	}
};
User users[] = {
	 User("111","111"),
	 User("222","111"),
	 User("333","111"),
	 User("444","111"),
	 User("555","111")
};
void split(const string& s, vector<string>& sv, const char flag = ' ') {
	sv.clear();
	istringstream iss(s);
	string temp;

	while (getline(iss, temp, flag)) {
		sv.push_back(temp);
	}
	return;
}
file files[100];
class FileSystem
{
public:
	FileSystem();
	~FileSystem();

	void WaitCommand();

	void DoCommand(string str);

public:
	void updatefile(string oldFile, string newfile) {
		for (size_t i = 0; i < 100; i++)
		{
			if (files[i].fileName == oldFile) {
				files[i].fileName = newfile;
				break;
			}
		}
	}
	bool exist(string file) {
		for (size_t i = 0; i < 100; i++)
		{
			if (files[i].fileName == file) {
				return true;
			}
		}
		return false;
	}
	class FileTree {
	public:

		enum NodeRelation
		{
			brother,
			child
		};
		enum NodeStatus
		{
			empty = 0,
			dir,
			file
		};
		typedef int NodePtr;
		static const NodePtr PtrNull = -1;
		static const NodePtr PtrRoot = 0;
		bool operator()(NodePtr ptr) {
			return ptr != PtrNull;
		}
		class Node {
		public:

			Node() {
				status = empty;
			}
			Node(string n, NodeStatus t) {
				name = n, status = t;
			}
			NodeStatus status = empty;
			string name = "";
			NodeRelation relation = NodeRelation::child;
			NodePtr preNode = PtrRoot;
			NodePtr child = PtrRoot;
			NodePtr brother = PtrRoot;
			bool haveChild() {
				return child > 0;
			}
			string display() {
				return name + " " + ((status == dir)?"\t<DIR>":"");
			}
			friend bool operator< (const Node& nodeA, const Node& nodeB) {
				return nodeA.name < nodeB.name;
			}
			friend bool operator> (const Node& nodeA, const Node& nodeB) {
				return nodeA.name > nodeB.name;
			}
			friend bool operator== (const Node& nodeA, const Node& nodeB) {
				return nodeA.name == nodeB.name;
			}
			friend ostream& operator<< (ostream& out,const Node node) {
				return out << node.status << "," << node.name << "," << node.relation << "," << node.preNode << ","<< node.child << "," << node.brother;
			}
			friend istream& operator>> (istream& in, Node& node) {
				//return out << node.name << " " << ((node.status == dir)?"\t<DIR>":"");
				string para;
				getline(in, para);
				vector<string> paras;
				split(para, paras, ',');
				node.status = (FileSystem::FileTree::NodeStatus)stoi(paras[0]);
				node.name = paras[1];
				node.relation = (FileSystem::FileTree::NodeRelation)stoi(paras[2]);
				node.preNode = stoi(paras[3]);
				node.child = stoi(paras[4]);
				node.brother = stoi(paras[5]);
				return in;
			}
		};

		FileTree() {
			Node root("root",dir);
			m_nodePool.push_back(root);
		}
	private:
		////////// var //////////
		vector<Node> m_nodePool;
		NodePtr workDir = PtrRoot;
		NodePtr curEmptyNodePtr = PtrRoot;

		////////// func //////////
		Node& NodeRef(NodePtr ptr) {
			if (ptr > m_nodePool.size()) {
				cerr << "NodeRef : " << ptr << ' ' << m_nodePool.size() << endl;
			}
			return m_nodePool.at(ptr);
		}
		NodePtr createNewNode(Node protype = *new Node()) {
			do {
				curEmptyNodePtr++;
				if (curEmptyNodePtr > ((int)m_nodePool.size() - 10)) {	// int 和 size_t 型不能正确比较 ? https ://bbs.csdn.net/topics/350204223
					for (int i = 0; i < 20; i++) {
						m_nodePool.push_back(*new Node);
					}
				}
			} while (!isEmpty(curEmptyNodePtr));

			NodeRef(curEmptyNodePtr) = protype;
			return curEmptyNodePtr;
		}
		void setRelationship(NodePtr start, NodePtr end, NodeRelation relation) {
			if (relation == child) {
				NodeRef(start).child = end;
				// if (end)
				NodeRef(end).preNode = start;
				NodeRef(end).relation = child;
			}
			else if(relation == brother)
			{
				NodeRef(start).brother = end;
				NodeRef(end).preNode = start;
				NodeRef(end).relation = brother;
			}
		}

		void showAllInDir(NodePtr dir,int depth = 0,bool recursion = true, ostream& out = cout) {
			for (int i = 0; i < depth; i++) {
				if (i < depth - 1)
					out << "┆   ";
				else
					if (getNode(dir).brother)
						out << "├─  ";
					else
						out << "└─  ";
			}

			out << getNode(dir).display() << endl;
			NodePtr subPtr = getNode(dir).child;
			while (subPtr && !isEmpty(subPtr)) {
				showAllInDir(subPtr,depth+1, recursion);
				subPtr = getNode(subPtr).brother;
			}
		}
		void showDir(NodePtr dir, ostream& out = cout) {
			showAllInDir(dir, 0, false);
		}
	public:

		int ReadFile(istream& in ,int count) {
			int nodeCounter = 0;
			m_nodePool.resize(count);
			for (int i = 0; i < count; i++) {
				in >> m_nodePool[i];
				if (m_nodePool[i].status != empty)
					nodeCounter++;
			}
			curEmptyNodePtr = 0;
			return nodeCounter;
		}
		void WriteFile(ostream& out) {
			out << m_nodePool.size() << endl;
			for (int i = 0; i < m_nodePool.size(); i++) {
				out << m_nodePool[i] << endl;
			}
		}

		bool isEmpty(Node node) {
			return node.status == empty;
		}
		bool isEmpty(NodePtr ptr) {

			return isEmpty(getNode(ptr));
		}		
		Node getNode(NodePtr ptr) {
			if (ptr < m_nodePool.size()) {
				return m_nodePool.at(ptr);
			}
			else
			{
				cerr << "cant get node " << ptr << endl;
			}
		}

		NodePtr findInDir(NodePtr source,string targetName) {
			NodePtr subPtr = getNode(source).child;
			while (subPtr && !isEmpty(subPtr)) {
				if (getNode(subPtr).name == targetName) {
					return subPtr;
				}
				subPtr = getNode(subPtr).brother;
			}
			return PtrNull;
		}
		
		void changeDirectory(NodePtr target) {
			workDir = target;
		}

		NodePtr getParent(NodePtr ptr) {
			while (getNode(ptr).relation == brother)
			{
				ptr = getNode(ptr).preNode;
			}
			return getNode(ptr).preNode;
		}

		string nodePtrToPath(NodePtr ptr) {
			string path = getNode(ptr).name;
			while (ptr) {
				NodePtr parent = getParent(ptr);
				path = getNode(parent).name + '/' + path;
				ptr = parent;
			}
			return path;
		}
		NodePtr pathToNodePtr(string path) {
			vector<string> sv;
			NodePtr target = workDir;
			split(path, sv, '/');
			bool init = false;
			for (const auto& s : sv) {
				if (!init) {
					if (s == "") {
						target = PtrRoot;
					}
					else if (s == "..") {
						target = getParent(target);
					}
					else if (s == ".")
					{
						target = workDir;
					}
					else
					{
						init = true;
					}
				}
				if (init) {
					NodePtr f = findInDir(target, s);
					if (f != PtrNull) {
						target = f;
					}
					else
					{
						return PtrNull;
					}
				}
			}
			return target;
		}
		void removeNode(NodePtr ptr) {
			NodePtr temp = getNode(ptr).brother;
			NodePtr p = getNode(ptr).preNode;
 			if (getNode(ptr).relation == brother) {
				setRelationship(p, temp, brother);
			}
			else if (getNode(ptr).relation == child) {
				setRelationship(p, temp, child);
			}
			NodeRef(ptr).status = empty;
		}
		NodePtr copyNode(NodePtr src, NodePtr parent,NodeRelation relation) {
			NodePtr newPtr = createNewNode(getNode(src));
			setRelationship(parent, newPtr, relation);
			Node newNode = getNode(newPtr);
			if (getNode(newPtr).child) {
				NodeRef(newPtr).child = copyNode(getNode(newPtr).child, newPtr, child);
			}
			if (getNode(newPtr).brother) {
				NodeRef(newPtr).brother = copyNode(getNode(newPtr).brother, newPtr, brother);
			}
			return newPtr;
		}
		/////////////////////////////////////////
		string GetWorkingDir() {
			return nodePtrToPath(workDir);
		}
		NodePtr GetWorkingDirPtr() {
			return workDir;
		}

		// attach a clone of newNode to source
		void AttachNode(NodePtr source,Node newNode, NodeRelation mode = child) {
			if (isEmpty(source)|| isEmpty(newNode)) {
				cerr << "cant attach "<< (isEmpty(newNode)?"empty":"") << " node " <<" to "<<(isEmpty(source)?"empty":"") <<" node " << source << endl;
				return;
			}
			NodePtr targetPtr = source;
			if (mode == brother)
			{
				targetPtr = getParent(targetPtr);
			}
			
			NodePtr n = createNewNode(newNode);
			if (!getNode(targetPtr).haveChild()) {
				setRelationship(targetPtr, n, child);
			}
			else
			{
				if ( getNode(n) < getNode(getNode(targetPtr).child)) {
					NodePtr temp = NodeRef(targetPtr).child;
					setRelationship(targetPtr, n, child);
					setRelationship(n, temp, brother);
				}
				else
				{
					targetPtr = getNode(targetPtr).child;
					NodePtr nextPtr;
					while (nextPtr = getNode(targetPtr).brother)
					{
						if (getNode(n) < getNode(nextPtr)) {
							setRelationship(targetPtr, n, brother);
							setRelationship(n, nextPtr, brother);
							return;
						}
						targetPtr = nextPtr;
					}
					setRelationship(targetPtr, n, brother);
				}
			}
		}

		void CreateNewFile(string name) {
			AttachNode(workDir,*new Node(name, file));
		}

		void CreateNewDir(string name) {
				AttachNode(workDir, *new Node(name, dir));
		}

		void RemoveDir(NodePtr ptr) {
			if (ptr && ptr != PtrNull) {
				removeNode(ptr);
			}
		}
		bool NotNull(NodePtr ptr) {
			return ptr != PtrNull;
		}

		bool RenameNode(NodePtr ptr,string newName) {
			if (ptr && NotNull(ptr)) {
				NodePtr parent = getParent(ptr);

				if (NotNull(findInDir(parent, newName)) ){
				}
				else
				{
					NodeRef(ptr).name = newName;
					return true;
				}
			}
			return false;
		}

		void CopyNode(NodePtr src,NodePtr dstParent) {
			NodePtr newNode = createNewNode(getNode(src));
			if (getNode(src).child) {
				copyNode(getNode(src).child, newNode, child);
			}
			AttachNode(dstParent, getNode(newNode), child);
		}

		void DisplayAll() {
			cout << endl;
			cout << "--- all context " << m_nodePool.size() << " ---" << endl;
			showAllInDir(PtrRoot);
			cout << endl;
		}
		void DisplayDir(NodePtr ptr = PtrNull) {
			if (ptr == PtrNull) {
				ptr = workDir;
			}
			//cout << "--- dir context"<< " ---" << endl;
			cout << endl;
			showDir(ptr);
			cout << endl;
		}
	};

	FileTree fileTree;
	bool isLogin;


	class Command {
	public:
		string head;
		string info;
	public:
		void ProcessCommand(FileSystem* pfs, string para) {
			if (para == "?") {
				cout << GetInfo() << endl;
			}
			else
			{
				this->DoCmd(pfs, para);
			}
		}
		void printError(string message) {
			cerr << "error:\t" << message << endl;
		}
		bool checkYN(string message) {
			cout << head + ":\t" << message + " Y/N" << endl;
			bool check = false;
			bool ret = false;
			string input;
			while (!check) {
				getline(cin, input);
				if (input == "Y" || input == "y") {
					check = true;
					ret = true;
				}
				else if (input == "N" || input == "n") {
					check = true;
					ret = false;
				}
				else
				{
					cout << head + ":\t" << message + " Y/N" << endl;
				}
			}
			return ret;
		}

		bool CheckFileName(FileSystem* pfs, string para, FileTree::NodeStatus status) {
			FileTree::NodePtr ptr = pfs->fileTree.pathToNodePtr(para);
			if (ptr == FileTree::PtrNull)printError("target dir not exist");
			else if (ptr == FileTree::PtrRoot)printError("cant operate root");
			else
			{
				return true;
			}
			return false;
		}

		// do command with para
		virtual void DoCmd(FileSystem* pfs, string para) = 0;
		string GetInfo() {
			return head + "\t│ " + info;
		}
		string GetHead() {
			return head;
		}

	};
	// base function
	void SaveData(){
		ofstream out("data.txt", ios::out | ios::trunc);
		if (out.fail()) {
			cout << "open file failed" << endl;
			cout << "write failed" << endl;
			return;
		}
		fileTree.WriteFile(out);
	}

	// command class
	class CmdHelp :public Command{
	public:
		CmdHelp(){	head = "help";	info = "显示帮助信息。";}
		void DoCmd(FileSystem* pfs,string para) {
			if (para == "") {
				vector<Command*>& commandVector = pfs->m_CommandVector;
				cout << endl;
				for (int i = 0; i < commandVector.size(); i++) {
					cout << commandVector.at(i)->GetInfo() << endl;
				}
				cout << endl;
			}
		}
	};
	class CmdShow :public Command {
	public:
		CmdShow(){	head = "showall";info = "显示所有节点";}
		void DoCmd(FileSystem* pfs, string para) {
			if (para == "") {
				pfs->fileTree.DisplayAll();
			}
		}
	};	
	class CmdNewFile :public Command {
	public:
		CmdNewFile() { head = "touch"; info = "创建新文件"; }
		void DoCmd(FileSystem* pfs, string para) {
			if (para.find('/') != -1) {
				printError("name can't contain '/' !");
				return;
			}
			if (para == "") {
				printError("no name appended!");
			}
			else
			{
				for (int i = 0; i < 100; i++)
				{
					if (files[i].fileName.empty())
					{
						files[i].fileName = para;
						break;
					}
				}
				FileTree::NodePtr ptr = pfs->fileTree.pathToNodePtr(para);
				if(ptr == FileTree::PtrNull)
				{
					pfs->fileTree.CreateNewFile(para);
				}
				else
				{
					printError("exists same name file");
				}

			}
		}
	};
	class CmdNewDir :public Command {
	public:
		CmdNewDir() { head = "mkdir"; info = "创建新目录。"; }
		void DoCmd(FileSystem* pfs, string para) {
			if (para == "") {
				printError("no name appended!");
				return;
			}
			if (para.find('/') != -1) {
				printError("name can't contain '/' !");
				return;
			}
			FileTree::NodePtr ptr = pfs->fileTree.pathToNodePtr(para);
			if (ptr == FileTree::PtrNull)
			{
				pfs->fileTree.CreateNewDir(para);
			}
			else
			{
				printError("exists same name dir");
			}
		}
	};
	class CmdRemoveDir :public Command {
	public:
		CmdRemoveDir() { head = "rmdir"; info = "删除空目录。"; }
		void DoCmd(FileSystem* pfs, string para) {
			if (para == "") {
				printError("no name appended!");
				return;
			}
			FileTree::NodePtr ptr = pfs->fileTree.pathToNodePtr(para);
			if(ptr == FileTree::PtrNull)printError("target dir not exist"); 
			else if (ptr == FileTree::PtrRoot)printError("cant remove root");
			else if (pfs->fileTree.getNode(ptr).haveChild() || pfs->fileTree.getNode(ptr).status != FileTree::NodeStatus::dir)printError(para + "is not a empty dir");
			else
			{
				pfs->fileTree.RemoveDir(ptr);
			}
		}
	};
	class CmdRemove :public Command {		// todo 文件或目录
	public:
		CmdRemove() { head = "rm"; info = "删除文件或目录。"; }
		void DoCmd(FileSystem* pfs, string para) {
			
			if (para == "") {
				printError("no name appended!");
				return;
			}
			FileTree::NodePtr ptr = pfs->fileTree.pathToNodePtr(para);
			if (ptr == FileTree::PtrNull)printError("target dir not exist");
			else if (ptr == FileTree::PtrRoot)printError("cant remove root");
			else
			{
				if (checkYN("是否删除\"" + para + "\"?")) {
					pfs->fileTree.RemoveDir(ptr);
				}
			}
		}
	};
	class CmdRename :public Command {
	public:
		CmdRename() { head = "mv"; info = "重命名文件或目录。"; }
		void DoCmd(FileSystem* pfs, string para) {
			vector<string> paras;
			split(para, paras, ' ');
			if (paras.size() != 2) {
				printError("paras appended not match");
				return;
			}
			if (CheckFileName(pfs,paras[0], FileTree::NodeStatus::file)) {

				FileTree::NodePtr ptr = pfs->fileTree.pathToNodePtr(paras[0]);
				pfs->updatefile(paras[0], paras[1]);
				if (!pfs->fileTree.RenameNode(ptr, paras[1])) {
					printError("same name exists");
				}
			}

		}
	};
	class CmdChangeDir :public Command {
	public:
		CmdChangeDir() { head = "cd";	info = "切换工作目录。"; }
		void DoCmd(FileSystem* pfs, string para) {
			if (para == "") {
				cerr << "no name appended!" << endl;
				return;
			}
			//FileTree::NodePtr target = pfs->fileTree.findInDir(para,FileTree::NodeStatus::dir);
			FileTree::NodePtr target = pfs->fileTree.pathToNodePtr(para);
			if (target != FileTree::PtrNull) { pfs->fileTree.changeDirectory(target); }
			else
			{
				cerr << "target dir not exist" << endl;
			}
		}
	};
	class CmdPrintWorkingDir :public Command {
	public:
		CmdPrintWorkingDir() { head = "pwd"; info = "显示用户当前所处的工作目录。"; }
		void DoCmd(FileSystem* pfs, string para) {
			if (para == "") {
				cout << pfs->fileTree.GetWorkingDir() << endl;
				return;
			}
		}
	};
	class CmdListDir:public Command {
	public:
		CmdListDir() { head = "ls"; info = "显示当前/指定目录下的内容。"; }
		void DoCmd(FileSystem* pfs, string para) {
			if (para == "") {
				pfs->fileTree.DisplayDir();
				return;
			}
			else
			{
				FileTree::NodePtr target = pfs->fileTree.pathToNodePtr(para);
				if (target != FileTree::PtrNull) { pfs->fileTree.DisplayDir(target); }
				else
				{
					cerr << "target dir not exist" << endl;
				}
			}
		}
	};
	class CmdCopyDir :public Command {
	public:
		CmdCopyDir() { head = "cp"; info = "复制文件和目录。"; }
		void DoCmd(FileSystem* pfs, string para) {
			vector<string> paras;
			split(para, paras, ' ');
			if (paras.size() != 2) {
				printError("paras appended not match");
				return;
			}
			else
			{
				FileTree::NodePtr src = pfs->fileTree.pathToNodePtr(paras[0]);
				FileTree::NodePtr dst = pfs->fileTree.pathToNodePtr(paras[1]);
				if (src != FileTree::PtrNull && dst != FileTree::PtrNull)
				{
					vector<string> sv;
					split(paras[0], sv, '/');
					FileTree::NodePtr ss = pfs->fileTree.findInDir(dst, sv[sv.size() - 1]);
					if (pfs->fileTree.findInDir(dst, sv[sv.size() - 1])!= -1) {
						cout << "file exist" << endl;
						return;
					}
					pfs->fileTree.CopyNode(src, dst);
				}
				else if (src == FileTree::PtrNull)
				{
					printError("src doesnt exists");
				}
				else if (dst == FileTree::PtrNull)
				{
					printError("dst doesnt exists");
				}
			}
		}
	};
	class CmdDebug:public Command {
	public:
		CmdDebug() { head = "exit"; info = "保存并退出程序。"; }
		void DoCmd(FileSystem* pfs, string para) {
			pfs->SaveData();
			exit(0);
		}
	};
	class CmdLogin:public Command {
	public:
		CmdLogin() {
			head = "login";info = "登陆。";
		}
		void DoCmd(FileSystem* pfs, string para) {
			string name, password;
			cout << "username:";
			cin >> name;
			cout << "password:";
			cin >> password;
			bool result = false;
			for (int i = 0; i < 5; i++)
			{
				User temp = users[i];
				if (temp.name == name && temp.password == password) {
					result = true;
					break;
				}
			}
			if (result == true) {
				cout << "登陆成功" << endl;
				pfs->isLogin = true;
			}
			else
			{
				cout << "登陆失败" << endl;
			}
		}
	};
	class CmdLogOut :public Command {
	public:
		CmdLogOut() {
			head = "logout";info = "登出。";
		}
		void DoCmd(FileSystem* pfs, string para) {
			cout << "登出成功" << endl;
			pfs->isLogin = false;
		}
	};
	class CmdEdit :public Command {
	public:
		CmdEdit() { head = "vim"; info = "打开与编辑文件。"; }
		void DoCmd(FileSystem* pfs, string para) {
			if (para.find('/') != -1) {
				printError("name can't contain '/' !");
				return;
			}
			if (para == "") {
				cout << "文件名为空" << endl;
			}
			else
			{
				int S = 1;
				for (int i = 0; i < 100; i++)
				{
					string content = files[i].content;
					string s;
						if (files[i].fileName == para) 
						{
							cout << "原文件的内容如下：" << endl;
							cout << files[i].content + "\n" << endl;
							cout << "请输入内容" << endl;
							getline(cin, s);
							if (s == "z")
							{
								break;
							}
							files[i].content = content + s;
							cout << "输入成功！现在文件内容如下：" << endl;
							cout << files[i].content + "\n" << endl;
						}

						S++;
				}
				

				if (S==100)
				{
					cout << "无法找到文件" << endl;
				}
			}
			
			
		}
	};
	vector<Command*> m_CommandVector;
};

// instance cmd
FileSystem::CmdHelp cmdhelp;
FileSystem::CmdShow cmdshow;
FileSystem::CmdNewFile cmdnewFile;
FileSystem::CmdNewDir cmdnewDir;
FileSystem::CmdChangeDir cmdchangeDir;
FileSystem::CmdPrintWorkingDir cmdprintWD;
FileSystem::CmdRemoveDir cmdremoveDir;
FileSystem::CmdListDir cmdlistDir;
FileSystem::CmdRename cmdrenameFile;
FileSystem::CmdRemove cmdRemove;
FileSystem::CmdCopyDir cmdCopy;
FileSystem::CmdDebug cmdDebug;
FileSystem::CmdLogin cmdLogin;
FileSystem::CmdLogOut cmdLogOut;
FileSystem::CmdEdit cmdEdit;

FileSystem::FileSystem()
{
	isLogin = false;
	cout << "loading..." << endl;
	// append command list
	m_CommandVector.push_back(&cmdLogin);
	m_CommandVector.push_back(&cmdLogOut);
	m_CommandVector.push_back(&cmdEdit);
	m_CommandVector.push_back(&cmdhelp);
	m_CommandVector.push_back(&cmdshow);
	m_CommandVector.push_back(&cmdnewFile);
	m_CommandVector.push_back(&cmdnewDir);
	m_CommandVector.push_back(&cmdchangeDir);
	m_CommandVector.push_back(&cmdprintWD);
	m_CommandVector.push_back(&cmdremoveDir);
	m_CommandVector.push_back(&cmdlistDir);
	m_CommandVector.push_back(&cmdrenameFile);	
	m_CommandVector.push_back(&cmdDebug);
	m_CommandVector.push_back(&cmdRemove);
	m_CommandVector.push_back(&cmdCopy);


	ifstream in("data.txt");
	if (in.fail()) {
		cout << "open file failed" << endl;
		cout << "initialization failed" << endl;
		return;
	}

	string temp;
	getline(in, temp);
	int count = stoi(temp);
	
	if (count) {
		int realCount = fileTree.ReadFile(in, count); 
		cout << "find " << realCount<< " nodes from file" << endl;
	}
	in.close();

	cout << "initialized" << endl;
}

FileSystem::~FileSystem()
{
	SaveData();
}

void FileSystem::WaitCommand() {
	//<< "<"
	cout << fileTree.GetWorkingDir() << ">";
}

void FileSystem::DoCommand(string str)
{
	bool findCmd = false;
	for(int i=0;i<m_CommandVector.size();i++)
	{
		Command* ptr = m_CommandVector[i];

		bool isHead = false;
		string head = ptr->head;

		if (str.substr(0, head.size()) != head) {
			
			isHead = false;
		}
		else
		{
			if (head == "login"||head=="help") {
				isHead = true;
			}
			else {
				// 这里判断你是否登陆不然不进行后面的工作
				if (isLogin == false) {
					cout << "没有登陆不能使用该功能" << endl;
					isHead = false;
				}
				else {
					isHead = true;
				}
			}



		}

		if (isHead) {
			string para = str.substr(ptr->GetHead().size());
			if (para.length() && para[0] == ' ') {
				para = para.substr(1);
				ptr->ProcessCommand(this, para);
				findCmd = true;
				break;
			}
			else if(!para.length())
			{
				ptr->ProcessCommand(this, para);
				findCmd = true;
				break;
			}
		}
	}
	if (!findCmd && str.length()) {
		cout << str + " 不是内部命令或用户命令。" << endl;
	}

}


// 演示用指令列表（一起输入会出错，要一条一条输入）
/*

help
ls ?
ls
mkdir tempDir
cd tempDir 
mkdir dir01
mkdir dir02
cd dir01
touch newFile.txt
ls ..
cd ..
cp dir01 /file
cd /file
showall
mv dir01 myWork
ls
rm myWork/newFile.txt
y
exit
*/